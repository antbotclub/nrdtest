# Python program to find the largest number among the three input numbers

# change the values of num1, num2 and num3
# for a different result
import nerdvision
nerdvision.start('cbd20afaae53e65d671752a5058cf67c08bfd5389a2acc1faf4db22b1af26764f872274fd22eb6416ae761af28edd71a2566d8b118b451d251998b46863ee671')

num1 = 10
num2 = 20
num3 = 12

# uncomment following lines to take three numbers from user
#num1 = float(input("Enter first number: "))
#num2 = float(input("Enter second number: "))
#num3 = float(input("Enter third number: "))

if (num1 >= num2) and (num1 >= num3):
   largest = num1
elif (num2 >= num1) and (num2 >= num3):
   largest = num2
else:
   largest = num3

print("The largest number is", largest)